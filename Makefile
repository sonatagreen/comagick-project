all: sprites comics

sprites: $(patsubst spritesheets/%.json, sprites/%, $(wildcard spritesheets/*.json))

sprites/%: spritesheets/%.json spritesheets/%.png
	@rm -r sprites/$*
	decompose-spritesheet $*

comics: $(patsubst comics/%.cmg, comics/%.png, $(wildcard comics/*.cmg))

comics/%.png: comics/%.cmg
	comagick $< $@
