# **Com**ag**ic**k
## comagick-project 0.2.1

Comagick (pronounced COM-magic) is an ImageMagick-based language for writing sprite comics.

This repository contains supporting files appropriate to a project using Comagick.

To get started using Comagick, follow the following steps:

1. Make sure you have the prerequisites installed: Python 3 and [Wand](http://docs.wand-py.org/en/0.4.1/).
2. Download this repository, and place the files `comagick` and `decompose-spritesheet` somewhere in your [PATH](https://en.wikipedia.org/wiki/PATH_%28variable%29).
3. Fork [comagick-project](https://bitbucket.org/sonatagreen/comagick-project).
4. Navigate to the comagick-project directory and run `make`.
5. Create a new file at `comics/<comicname>.cmg`, or open up `comics/example.cmg`.

----

comagick-project includes sprites by several artists under a variety of libre licenses. See LICENSE for more detailed information.

----

Usage: 

    make

----

A .cmg file consists of a _prologue_, which loads sprites and sets other options, followed by a series of _directives_, which place sprites and create dialogue and other text.

To load a sprite, use a line of the form `@name: image scale`.

* The leading `@` indicates that you are loading a sprite.
* `name` is the short name you'll use to refer to this sprite elsewhere in this file.
* `image` indicates where to find the image file; Comagick expands this to `sprites/image.png`.
* `scale` is an integer by which the image will be scaled up. To use the image without scaling, put a 1.

To set an option, use a line of the form `option: value`.  
`value` will be parsed as JSON. Note that JSON requires strings to be delimited with "double quotes", not 'single quotes'.

All options are optional. Supported options are:

* font: "fontname"
* font_size: int (16)
* text_antialias: **true**|false
* text_adjust: [x,y]
* ballooncolor: "black"
* background: "white"
* padding: int (8)
* margincolor: "white"
* marginsize: int (4)
* bordercolor: "black"
* bordersize: int (2)
* panelsize: int

----

To start a new row of panels (including the first/only row, marking the end of the prologue), put a `#` on a line by itself. Separate panels within a row by a blank line. To create an empty panel, put a `-` on a line by itself. A `.` on a line by itself is a vertical spacer.

The first character of a standard directive indicates the _position_: `l`eft, `c`enter, or `r`ight. The second character is an _operator_, which controls the interpretation of the rest of the directive.

* `c!name`  
Place the sprite named _name_ at the specified position.
* `c: This is some text.`  
Create a speech bubble with the given text in the given position.
* `c!name This is some text.`  
This is a convenience shorthand to draw both a sprite and a speech bubble in a single directive.
* `c" This is some text.`  
Create a speech bubble without a tail. (offscreen speaker)
* `c> This is some text.`  
Create a text box with sharp corners. (narrator or caption)
* `c* This is some text.`  
Create text with no box. (sound fx)

---

The example comic ([comics/example.cmg](https://bitbucket.org/sonatagreen/comagick-project/src/master/comics/example.cmg)) is shown below.

![example comic](http://sonatagreen.com/static/example-small.png)
